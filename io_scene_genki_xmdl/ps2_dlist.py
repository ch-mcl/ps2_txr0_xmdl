import array
import io
import struct


class VIF_COMMAND:
    fmt = '1H2B'

    def __init__(self):
        self.immediate = 0x00
        self.count = 0x00
        self.command = 0x00 # NOP

    def unpack(self, file):
        #print( 'File pos (vif command): {0:#X}'.format(file.tell()) )
        bytes = file.read(struct.calcsize(self.fmt))
        buff = struct.unpack_from(self.fmt, bytes, 0)

        self.immediate = buff[0]
        self.count = buff[1]
        self.command = buff[2]


class VIF_Vector:
    fmt_list = [ ['3i', 3], ['5h', 3], ['8b', 3], [],       # 0x00(0x60) S (Signed? count is 3only)
                 ['2f', 2], ['2h', 2], ['2b', 2], [],       # 0x04(0x64) V2(Vector2)
                 ['3f', 3], ['3h', 3], ['3b', 3], [],       # 0x08(0x68) V3(Vector3)
                 ['4f', 4], ['4h', 4], ['4b', 4], ['1i', 4] # 0x0C(0x6C) V4(Vector4)
    ]
    
    def __init__(self):
        elements = []
    
    def unpack(self, file, vif_command):
        type = (vif_command & 0x7F) - 0x60
        #print( '{0:#X}'.format(type) )
        fmt = self.fmt_list[type]
        bytes = file.read( struct.calcsize(fmt[0]) )
        buff = struct.unpack_from(fmt[0], bytes, 0)
        count = fmt[1]
        
        if (vif_command > 0x63):
            # 32bit float
            if (type%4 == 0):
                self.elements = [ buff[x] for x in range(count) ]
            # 16bit FixedPoint
            elif(type%4 == 1):
                self.elements = [ buff[x] for x in range(count) ]
            # 8bit FixedPoint
            elif(type%4 == 2):
                self.elements = [ buff[x] for x in range(count) ]
            # 5bit FixedPoint...?
            elif(type%4 == 3):
                return # How?
        else:
            # 5bit
            if (_type%4 == 3):
                return # How?
            # 32bit, 16bit, 8bit
            else:
                self.elements = [ buff[x] for x in range(count) ]


class DisplayList:
    def __init__(self):
        self.command = VIF_COMMAND()
        self.vector = []

    def unpack(self, file):
        vif_cmd = VIF_COMMAND()
        vif_cmd.unpack(file)

        if vif_cmd.command > 0x5F:
            vectors = [ VIF_Vector() for x in range(vif_cmd.count) ]
            for vec in vectors:
                vec.unpack(file, vif_cmd.command)
                self.vector.append(vec)