import array
import mathutils

import bmesh
import bpy

from .ps2_dlist import DisplayList, VIF_Vector
from .xmdl import XMDL, Mesh

CHAR_SET = 'ascii'

#Names
NAME_MATERIAL = 'material_{0:03}'

#Messages
MSG_WARN_NOT_XMDL = 'magic_str:{0} not match magic:{1}'
MSG_INFO_DATA = '{0}: {1}'
MSG_INFO_DATA_HEX = '{0}: {1:#X}'

#FiXed-point divide value
fx = 1<<15

#Storage Mesh datas
class Mesh_data:
    def __init__(self):
        self.normals = [] #Collect for "Custom Split Normals"
        #self.color0s = []
        self.uvs = []


def generate_mesh(mesh :Mesh, idx: int):
    mesh_name = 'mesh_{0:04}'.format(idx)
    #print("---- Generate {0} ---".format(mesh_name))
    bl_mesh = bpy.data.meshes.new(mesh_name)
    bl_obj = bpy.data.objects.new(mesh_name, bl_mesh)

    bpy.context.collection.objects.link(bl_obj)
    bpy.context.view_layer.objects.active = bl_obj
    bl_obj.select_set(True)
    
    bl_mesh = bpy.context.object.data
    bm = bmesh.new()
    
    v0 = mathutils.Vector(( 0, 0, 0 ))
    v1 = mathutils.Vector(( 0, 0, 0 ))
    v2 = mathutils.Vector(( 0, 0, 0 ))
    
    uv_default = VIF_Vector()
    uv_default.elements = [ 0, 0 ]
    normal_default = VIF_Vector()
    normal_default.elements = [ 0, 0, 0, 0 ]
    
    mesh_data = Mesh_data() # Store Normals, UVs, VertexColors, etc
    for submesh_cnt, submesh in enumerate(mesh.submeshs):
        #print('--- new submesh ---')
        sur_cnt = 0
        for surface in submesh.surfaces:
            is_cw = False # is Clock Wise True:Clock Wise Flase:Counter Closk Wise
            vtx_cnt = 0
            #print('--- new surface {0}---'.format(sur_cnt))
            #Position
            pos_vecs = surface.dlists[3].vector
            pos_vecs_len = len(pos_vecs)
            #UV
            uv_vecs = surface.dlists[4].vector
            uv_vecs_len = len(uv_vecs)
            #Normal
            normal_vecs = surface.dlists[5].vector
            normal_vecs_len = len(normal_vecs)
            max_len = max(pos_vecs_len, uv_vecs_len, normal_vecs_len)
            
            if uv_vecs_len < max_len:
                for i in range(max_len - uv_vecs_len):
                    uv_vecs.append(uv_default)
            
            if normal_vecs_len < max_len:
                for i in range(max_len - normal_vecs_len):
                    normal_vecs.append(normal_default)
            
            
            for pos_vec, uv_vec, normal_vec in zip (pos_vecs, uv_vecs, normal_vecs):
                #Vertex
                vtx = mathutils.Vector((pos_vec.elements[0], \
                                        pos_vec.elements[1], \
                                        pos_vec.elements[2]))
                v = bm.verts.new(vtx)

                #UV
                uv = mathutils.Vector(( uv_vec.elements[0], \
                                        uv_vec.elements[1] ))
                mesh_data.uvs.append(uv)

                #Normal
                normal = mathutils.Vector(( normal_vec.elements[0]/fx, \
                                            normal_vec.elements[1]/fx, \
                                            normal_vec.elements[2]/fx, \
                                            normal_vec.elements[3]/fx ))
                normal = normal.to_3d()
                #v.normal = normal
                mesh_data.normals.append(normal)

                #Generate Face
                if vtx_cnt > 1:
                    #print('is_cs: {0}'.format(is_cw))
                    v2 = v
                    if is_cw == True:
                        face = bm.faces.new((v2, v1, v0))
                        is_cw = False
                    else:
                        face = bm.faces.new((v0, v1, v2))
                        is_cw = True
                    face.material_index = submesh_cnt
                    
                    if vtx_cnt == 2:
                        #compare blender face and xmdl normal
                        xmdl_normals = mesh_data.normals[-3:]
                        xmdl_face_normal = sum(xmdl_normals, mathutils.Vector()) / 3.0
                        _bl_face_normal = mathutils.geometry.normal(v0.co, v1.co, v2.co)
                        dot_res = _bl_face_normal.dot(xmdl_face_normal)
                        #print('xmdl_face_normal: {0}, _bl_face_normal:{1}, Dot Product : {2}'.format(xmdl_face_normal, _bl_face_normal, dot_res))
                        if (dot_res < 0):
                            #flipping generated face
                            face.normal_flip()
                            #inversing next faces
                            is_cw = False if is_cw else True
                    
                    v0 = v1
                    v1 = v2
                elif vtx_cnt == 1:
                    v1 = v
                elif vtx_cnt == 0:
                    v0 = v
                vtx_cnt = vtx_cnt + 1
            sur_cnt = sur_cnt + 1
        
        #Material
        bl_mat = bpy.data.materials.new(name=NAME_MATERIAL.format(submesh_cnt))
        bl_obj.data.materials.append(bl_mat)

    bm.to_mesh(bl_mesh)
    bm.free()
    
    #uv
    channel_name = "uv0"
    try:
        bl_mesh.uv_layers[channel_name].data
    except:
        #Generate UV
        bl_mesh.uv_layers.new(name = channel_name)
    for i, loop in enumerate(bl_mesh.loops):
        bl_mesh.uv_layers[channel_name].data[i].uv = mesh_data.uvs[loop.vertex_index]
    
    #normal
    bl_mesh.polygons.foreach_set("use_smooth", [True] * len(bl_mesh.polygons))
    bl_mesh.use_auto_smooth = True
    bl_mesh.normals_split_custom_set_from_vertices(mesh_data.normals)

    #Object Mode
    bpy.ops.object.mode_set(mode='OBJECT', toggle=False)
    bpy.context.object.rotation_euler[0] = -1.5708


def load(filepath: str):
    # open files
    with open(filepath, 'rb') as file:
        xmdl = XMDL()
        xmdl.unpack(file)
        
        # Generate Mesh
        for idx, mesh in enumerate(xmdl.meshs):
            generate_mesh(mesh, idx)
        
    file.close()