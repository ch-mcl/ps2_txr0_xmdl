import array
import io
import struct

from .ps2_dlist import DisplayList

CHAR_SET = 'ascii'

#Names
NAME_MATERIAL = 'material_{0:03}'

#Messages
MSG_WARN_NOT_XMDL = 'magic_str:{0} not match magic:{1}'
MSG_INFO_DATA = '{0}: {1}'
MSG_INFO_DATA_HEX = '{0}: {1:#X}'


class Sruface:
    fmt = '1H2B3I'
    # 1H  2B  3I
    # 0   1-2 3-5
    
    def __init__(self):
        self.count_row = 0x00
        self.count_vertex = 0x00
        self.dlists = []

    def unpack(self, file):
        #print( MSG_INFO_DATA_HEX.format('File pos(Surface)', file.tell()) )
        bytes = file.read(struct.calcsize(self.fmt))
        buff = struct.unpack_from(self.fmt, bytes, 0)

        self.count_row = buff[0]
        self.count_vertex = buff[5]
        
        #print( MSG_INFO_DATA_HEX.format('count_row', self.count_row) )
        #print( MSG_INFO_DATA_HEX.format('count_vertex', self.count_vertex) )
        #print( MSG_INFO_DATA_HEX.format('File Position (dlist start)', file.tell()) )
        dlist_end_adr = file.tell() + (self.count_row * 0x10)
        #print( MSG_INFO_DATA_HEX.format('dlist_end_adr', dlist_end_adr) )        
        while ( file.tell() < dlist_end_adr ):
            dlist = DisplayList()
            dlist.unpack(file)
            self.dlists.append(dlist)


class Submesh:
    fmt = '4i' + '7f17I' + '3I9f'
    # 4i 7f   17I   3I    9f
    # 0  4-10 11-27 28-30 31-39
    
    def __init__(self):
        self.count_surface = 0x00
        self.size_dlist_total = 0x00
        self.surfaces = []

    def unpack(self, file):
        #print( 'File pos(Submesh): {0:#X}'.format(file.tell()) )
        bytes = file.read(struct.calcsize(self.fmt))
        buff = struct.unpack_from(self.fmt, bytes, 0)

        self.count_surface = buff[29]
        self.size_dlist_total = buff[30]
        #print( MSG_INFO_DATA_HEX.format('count_surface', self.count_surface) )        
        #print( MSG_INFO_DATA_HEX.format('size_dlist_total', self.size_dlist_total) )

        for i in range(self.count_surface):
            surface = Sruface()
            surface.unpack(file)
            self.surfaces.append(surface)
        
        file.seek(0x04, io.SEEK_CUR)
        while ( (file.tell()%0x10) > 0x00 ):
            file.seek(0x01, io.SEEK_CUR)


class SubmeshHeader:
    fmt = '4i'
    
    def __init__(self):
        self.count_submesh = 0x00
        #self.aabb = AABB

    def unpack(self, file):
        bytes = file.read(struct.calcsize(self.fmt))
        buff = struct.unpack_from(self.fmt, bytes, 0)

        self.count_submesh = buff[3]
        
        #print( MSG_INFO_DATA_HEX.format('submesh Count', self.count_submesh) )        
        # skip AABB
        file.seek(0x20, io.SEEK_CUR)


class Mesh:
    fmt = '<3I2H' + '4I' + '12f'
    # 3I   2H  4I  12f
    #  0-2 3-4 5-8 9-20
    
    def __init__(self):
        self.type = 0x00
        self.submeshs = []

    def unpack(self, file):
        #print( MSG_INFO_DATA_HEX.format('File pos(Mesh)', file.tell()) )
        bytes = file.read(struct.calcsize(self.fmt))
        buff = struct.unpack_from(self.fmt, bytes, 0)

        self.type = buff[0]
        _type = self.type & 0xF0
        
        if (_type == 0x30 or _type == 0x50 or _type == 0x70):
            submesh_header = SubmeshHeader()
            submesh_header.unpack(file)
            
            for i in range(submesh_header.count_submesh):
                #print( '--- surface idx: {0} ---'.format(i) )                
                submesh = Submesh()
                submesh.unpack(file)
                self.submeshs.append(submesh)


class XMDL:
    magic = 'XVI0'
    fmt = '<4s4s8B' + '4s4s4s4s' + '4i'
    # 4s 4s 8b   4s 4s 4s 4s 4i
    #  0  1  2-9 10 11 12 13 14~17
    
    def __init__(self):
        self.count_unk0x00 = 0x00
        self.count_mesh = 0x00
        self.unk_0x28 = 0x00
        self.unk_0x2c = 0x00
        #self.unk0x00s = []
        self.meshs = []

    def unpack(self, file):
        bytes = file.read(struct.calcsize(self.fmt))
        buff = struct.unpack_from(self.fmt, bytes, 0)
        
        magic_str = buff[0].decode(CHAR_SET)
        if magic_str[::-1] != self.magic:
            print(MSG_WARN_NOT_XMDL.format(self.magic_str, self.magic))
            return
        else:
            self.count_unk0x00 = buff[14]
            self.count_mesh = buff[15]
            self.unk_0x28 = buff[16]
            self.unk_0x2c = buff[17]
            if (self.unk_0x2c > 0x00):
                file.seek(self.unk_0x2c, io.SEEK_CUR)
                while ( (file.tell()%0x10) > 0x00 ):
                    file.seek(0x01, io.SEEK_CUR)
            #print( MSG_INFO_DATA_HEX.format('File pos(header)', file.tell()) )
                            
            #print("---- unk0x00 Count {0} ---".format(self.count_unk0x00))
            #skip "Unknown0x00" section
            file.seek(0x30 * self.count_unk0x00, io.SEEK_CUR)

            #print( MSG_INFO_DATA_HEX.format('File pos(header)', file.tell()) )

            #print("---- Mesh Count {0} ---".format(self.count_mesh))
            for i in range(self.count_mesh):
                mesh = Mesh()
                mesh.unpack(file)
                self.meshs.append(mesh)
                #print("---- Submesh Count {0} ---".format(len(mesh.submeshs)))