import bpy
import importlib

from . import import_xmdl

bl_info= {
    "name": "Genki PS2 Era 3D Model (.xmdl)",
    "author": "chmcl95",
    "version": (0, 1, 0),
    "blender": (2, 80, 0),
    "location": "File > Import > Genki Model (.xmdl)",
    "description": "Imports a Genki's PS2 Era 3D Model.",
    "category": "Import",
}


if "bpy" in locals():
    import importlib
    if "import_xmdl" in locals():
        importlib.reload(import_xmdl)

from bpy.props import (
         StringProperty,
         )


#Import
class IMPORT_SCENE_MT_GENKIXMDL(bpy.types.Operator):
    bl_idname = "import_scene.genki_xmdl"
    bl_label = "Import XMDL"
    bl_description = "Imports a Genki PS2 Era 3D Model"
    bl_options = {'REGISTER', 'UNDO'}

    filepath: StringProperty(
        name="File Path",
        description="Filepath used for importing the Genki PS2 Era 3D Model file",
        maxlen=1024)
    filter_glob: bpy.props.StringProperty(default="*.xmdl;*.xvi;*.xvi0", options={'HIDDEN'})

    def execute(self, context):
        keywords = self.as_keywords()
        import_xmdl.load(self.filepath)
        return {'FINISHED'}

    def invoke(self, context, event):
        wm = context.window_manager
        wm.fileselect_add(self)
        return {'RUNNING_MODAL'}


def menu_func_import(self, context):
    self.layout.operator(IMPORT_SCENE_MT_GENKIXMDL.bl_idname, text="Genki PS2 Era Model (.xmdl)")


classes = (
    IMPORT_SCENE_MT_GENKIXMDL,
)


def register():
    for cls in classes:
        bpy.utils.register_class(cls)
    
    bpy.types.TOPBAR_MT_file_import.append(menu_func_import)


def unregister():
    bpy.types.TOPBAR_MT_file_import.remove(menu_func_import)
    
    for cls in classes:
        bpy.utils.unregister_class(cls)


if __name__ == "__main__":
    register()