# Structure
- Header
- Unknown0[]
- Mesh[]

# Header
Section size is 48 Byte. Stores 3 Meta Informations and Count of Unknown0 and Model.

| Offset | Type      | Description            |
| ------ | --------- | ---------------------- |
| 0x00   | `Meta`    | Meta Information of XVI0.     |
| 0x08   | `byte[8]` | Padding 1.             |
| 0x10   | `Meta`    | Meta Information of XMDL.     |
| 0x18   | `char[4]` | Meta Information of NORM.     |
| 0x20   | `int`     | count of Unknown0.     |
| 0x24   | `int`     | count of Model Objects |
| 0x28   | `byte[8]` | Padding 2.             |

## Meta
| Offset | Type      | Description      |
| ------ | --------- | ---------------- |
| 0x00   | `char[4]` | Information text |
| 0x04   | `char[4]` | version          |

It seems to these are reversed. If reverse thses You can get `XVI0`, `1.00`, `XMDL`, `2.00`, `NORM` and `2.00`.


# Unknown0
Section size is 48 Byte. 


# Mesh
Section size is 80(0x50) byte or 128(0x80) byte. When type is 0x30, 0x50, 070  Scetion size will be 128 byte. And Array of Submesh will be exist After.
| Offset | Type         | Description          |
| ------ | ------------ | -------------------- |
| 0x00   | `uint`       | type                 |
| 0x04   | `uint`       | unknown.             |
| 0x08   | `uint`       | unknown.             |
| 0x10   | `uint`       | unknown.             |
| 0x14   | `uint`       | unknown.             |
| 0x18   | `uint`       | unknown.             |
| 0x1C   | `uint`       | unknown.             |
| 0x20   | `Vector4[3]` | Array of Vector4.    |
| 0x50   | `uint`       | unknown.             |
| 0x54   | `uint`       | unknown.             |
| 0x58   | `uint`       | unknown.             |
| 0x5C   | `uint`       | Count of Submesh.    |
| 0x60   | `AABB`       | Bounding Box.        |

## Submesh
| Offset | Type         | Description          |
| ------ | ------------ | -------------------- |
| 0x00   | `uint`       | unknown.             |
| 0x04   | `uint`       | unknown.             |
| 0x08   | `uint`       | unknown.             |
| 0x10   | `float`      | unknown.             |
| 0x14   | `float`      | unknown.             |
| 0x18   | `float`      | unknown.             |
| 0x1C   | `float`      | unknown.             |
| 0x20   | `float`      | unknown.             |
| 0x24   | `float`      | unknown.             |
| 0x28   | `float`      | unknown.             |
| 0x2C   | `uint`       | unknown.             |
| 0x30   | `uint`       | unknown.             |
| 0x34   | `uint`       | unknown.             |
| 0x38   | `uint`       | unknown.             |
| 0x3C   | `uint`       | unknown.             |
| 0x40   | `uint`       | unknown.             |
| 0x44   | `uint`       | unknown.             |
| 0x48   | `uint`       | unknown.             |
| 0x4C   | `uint`       | unknown.             |
| 0x50   | `uint`       | unknown.             |
| 0x54   | `uint`       | unknown.             |
| 0x58   | `uint`       | unknown.             |
| 0x5C   | `uint`       | unknown.             |
| 0x60   | `uint`       | unknown.             |
| 0x64   | `uint`       | unknown.             |
| 0x68   | `uint`       | unknown.             |
| 0x68   | `uint`       | unknown. constant 0xCCCCCCCC. |


